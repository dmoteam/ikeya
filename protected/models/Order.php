<?php

/**
 * This is the model class for table "order".
 *
 * The followings are the available columns in table 'order':
 * @property integer $order_id
 * @property string $created
 * @property string $invoice_id
 */
class Order extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order';
	}

	public function behaviors() {
		return array(
			'CTimestampBehavior' => array(
				'class'           => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'created',
				'updateAttribute' => 'updated',
			),
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fullname,email,phone,delivery_address', 'required'),

			array( 'fullname,email', 'length', 'max' => 255 ),

			array('email', 'email'),

			//array('invoice_id', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('order_id, created, invoice_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'products'     => array( self::HAS_MANY, 'OrderProduct', 'order_id' ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		//TODO localize
		return array(
			'order_id' => 'Order',
			'fullname' => 'Имя и Фамилия',
			'delivery_address' => 'Адрес доставки',
			'phone' => 'Телефон',
			'created' => 'Created',
			'invoice_id' => 'Invoice',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('order_id',$this->order_id);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('invoice_id',$this->invoice_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Order the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * @param $not_settled  if order is placed or not
	 * @return float
	 *
	 * If order is place then get producs total from order_product table
	 * otherwise from CWebUser state
	 */
	public function getOrder( $not_settled = true ){
		return ($not_settled) ? $this->_getUnsettledOrder() : $this->_getSettledOrder();
	}

	public function getOrderProducts( $not_settled = true ){
		return ($not_settled) ? $this->_getUnsettledOrderProducts() : $this->_getSettledOrderProducts();
	}

	public function _getSettledOrderProducts( ){
		$this->order_id = 1; //TODO remove this
		$products_ids = Yii::app()->db->createCommand("SELECT product_id FROM order_product WHERE order_id=:order_id")->queryAll(true, array(':order_id'=>$this->order_id));
		$_pids = array();

		if (!empty($products_ids)) {
			foreach ( $products_ids as $i => $pid ) {
				$_pids[ $i ] = (int) $pid['product_id'];
			}
		}

		return array(
			'counts'   => (array) array_count_values( $_pids ),
			'products' => (array) $this->_getProductsData( $_pids ),
		);

	}

	public function _getUnsettledOrderProducts( ){
		$products_ids = (array)Yii::app()->user->getState( 'products', array() );

		//if (!empty($products_ids)) {
		return array(
			'counts' => (array)array_count_values($products_ids),
			'products' => (array)$this->_getProductsData($products_ids)
		);
		//}
	}

	public function _getProductsData( $products_ids ){
		$pids = implode("','", $products_ids);
		return Yii::app()->db->createCommand("SELECT * FROM product WHERE id in ('$pids')")->queryAll(true);
	}

	private function _getUnsettledOrder(){
		$total = 0;
		$products_ids = Yii::app()->user->getState( 'products', array() );

		if (!empty($products_ids)) {
			$prices = $this->getProductsPrices($products_ids);

			foreach ( $products_ids as $pid ) {
				$total += $prices[$pid];
			}
		}
		return array(
			'total' => $total,
			'count' => count($products_ids),
		);
	}

	private function _getSettledOrder(){
		$this->order_id = 1; //TODO remove this
		$total = Yii::app()->db->createCommand("SELECT sum(price) as total, count(*) as count FROM order_product WHERE order_id=:order_id")->queryRow(true, array(':order_id'=>$this->order_id));
		return $total;
	}

	private function getProductsPrices($products_ids) {
		$result = array();

		foreach ( $products_ids as $i=>$pid ) {
			$products_ids[$i] = (int)$pid;
		}
		$pids = implode("','", $products_ids);

		$products = Yii::app()->db->createCommand("SELECT id, price FROM product WHERE id in ('$pids')")->queryAll(true);

		foreach ( $products as $product ) {
			$result[$product['id']] = $product['price'];
		}

		return $result;
	}

	public function afterSave(){
		$products = $this->_getUnsettledOrderProducts();
		foreach ( $products['products'] as $product ) {
			$order_product = new OrderProduct();
			$order_product->order_id = $this->order_id;
			$order_product->product_id = $product['id'];
			$order_product->product_name = $product['name'];
			$order_product->price = $product['price'];
			$order_product->quantity = (int)$products['counts'][$product['id']];
			$order_product->save();
		}
	}

}