<?php

/**
 * This is the model class for table "product".
 *
 * The followings are the available columns in table 'product':
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $type
 * @property string $description
 * @property string $image
 * @property string $price
 * @property string $created
 * @property string $updated
 *
 * The followings are the available model relations:
 * @property ProductProperties $id0
 * @property ProductImage[] $productImages
 * @property ProductProperties[] $productProperties
 */
class Product extends CActiveRecord {
	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'product';
	}

	public function behaviors() {
		return array(
			'CTimestampBehavior' => array(
				'class'           => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'created',
				'updateAttribute' => 'updated',
			),
			'sluggable'          => array(
				'class'        => (get_class(Yii::app()) != 'CConsoleApplication' ?'ext.behaviors.SluggableBehavior.SluggableBehavior':'ext.behaviors.SluggableBehavior.ConsoleSluggableBehavior'),
				'columns'      => array( 'productCategory.slug', 'name' ),
				'unique'       => true,
				'update'       => false,
				'toLower'      => true,
				'useInflector' => true,
			),
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array( 'name,price,image', 'required' ),
			array( 'slug', 'safe' ),
			array( 'ikea_link', 'safe' ),
			array( 'description', 'safe' ),
			array( 'id', 'numerical', 'integerOnly' => true ),
			array( 'category_id', 'numerical', 'integerOnly' => true ),
			array( 'name, type', 'length', 'max' => 255 ),
			array( 'id, category_id, name, slug, type, description, created, updated', 'safe', 'on' => 'search' ),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'productCategory'   => array( self::BELONGS_TO, 'Category', 'category_id' ),
			'productImages'     => array( self::HAS_MANY, 'ProductImage', 'product_id' ),
			'productProperties' => array( self::HAS_MANY, 'ProductProperties', 'product_id' ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id'          => 'ID',
			'category_id' => 'Category',
			'name'        => 'Name',
			'slug'        => 'Slug',
			'type'        => 'Type',
			'description' => 'Description',
			'image'       => 'Image',
			'price'       => 'Price',
			'price_special'       => 'Special Price',
			'created'     => 'Created',
			'updated'     => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare( 'id', $this->id );
		$criteria->compare( 'Category.id', $this->category_id, true );
		$criteria->compare( 'name', $this->name, true );
		$criteria->compare( 'slug', $this->slug, true );
		$criteria->compare( 'type', $this->type, true );
		$criteria->compare( 'description', $this->description, true );
		$criteria->compare( 'image', $this->image, true );
		$criteria->compare( 'price', $this->price, true );
		$criteria->compare( 'created', $this->created, true );
		$criteria->compare( 'updated', $this->updated, true );

		return new CActiveDataProvider( $this, array(
			'criteria' => $criteria,
		) );
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 *
	 * @param string $className active record class name.
	 *
	 * @return Product the static model class
	 */
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}

	public static function getHitProduct( $limit = 5 ) {
		$dataProvider = new CActiveDataProvider( __CLASS__, array(
			'criteria'      => array(
				'alias'  => 'p',
				'select' => 'p.*, (select count(*) from order_product op where op.product_id=p.id) as hot',
				//'condition'=>'status=1',
				'order'  => 'hot DESC, name ASC',
				//'with'=>array('author'),
				'limit'  => $limit
			),
			'countCriteria' => array(
				//'condition'=>'status=1',
				// 'order' and 'with' clauses have no meaning for the count query
			),
			'pagination'    => array(
				'pageSize' => $limit,
			),
		) );

		return $dataProvider->getData();
	}


	public static function getNewProduct( $limit = 5 ) {
		$dataProvider = new CActiveDataProvider( __CLASS__, array(
			'criteria'      => array(
				'alias' => 'p',
				//'select' => 'p.*, (select count(*) from order_product op where op.product_id=p.id) as hot',
				//'condition'=>'status=1',
				'order' => 'created DESC, updated DESC',
				//'with'=>array('author'),
				'limit' => $limit
			),
			'countCriteria' => array(
				//'condition'=>'status=1',
				// 'order' and 'with' clauses have no meaning for the count query
			),
			'pagination'    => array(
				'pageSize' => $limit,
			),
		) );

		return $dataProvider->getData();
	}

	public function findBySlug( $slug ) {
		return $this->find( 'slug=:slug', array( ':slug' => $slug ) );
	}

	public function getCategoryProducts( $category_id ) {
		$category_id = (int)$category_id;

		$dependency = new CDbCacheDependency( 'SELECT MAX(created) FROM product' );

		return new CActiveDataProvider( Product::model()->cache( 3600 * 24, $dependency, 2 ), array(
			'criteria'   => array(
				'condition' => "category_id = $category_id",
				'order'     => 'created DESC',
			),
			'pagination' => array(
				'pageSize' => 6,
			),
		) );
	}
}