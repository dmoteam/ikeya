<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	public $order=array(
		'count'=>0,
		'total'=>0,
	);


	/**
	 * Метод для возвращения форматированного значения валюты
	 * @param $amount
	 * @param string $locale  TODO
	 *
	 * @return string
	 */
	public function getFormattedPrice($amount, $locale='ru') {
		return number_format($amount, 0, '.', ',') . '<span class="currency">грн</span>';
	}

	protected function beforeRender( $view ) {
		// TODO error check and handling
		try {
			$order          = Order::model()->getOrder( true );
			$order['total'] = $this->getFormattedPrice( $order['total'] );
			$this->order    = $order;
		} catch(Exception $e) {

		}
		return parent::beforeRender( $view );
	}


}