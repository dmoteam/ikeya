<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',

	// preloading 'log' component
	'preload'=>array('log'),

	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.extensions..*',
	),

	// application components
	'components'=>array(
		'db'=> array(
			'connectionString' => 'mysql:host=localhost;dbname=DMO_ikea',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'pass123',
			'charset' => 'utf8',
			'enableParamLogging' => true,
			'enableProfiling' => true,
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),

		'transliteration' => array(
			'class' => 'application.components.Transliteration'
		),
	),
);