<?php
/**
 * @var $this OrderController
 * @var $order array
 * @var $model Order
 */
?>
<?php
function _drawErrorMessage($model, $attribute) {
	return sprintf('<div id="%s_em_" class="error">&nbsp;</div>', CHtml::activeId($model, $attribute));
}
?>
<div class="cart-view">
	<div class="row">
		<div class="col-md-16 order-header">
			<h1 class="">Заказ</h1>
		</div>
	</div>
	<?php if (!empty($products['products'])) { ?>

	<div class="row">
		<div class="col-md-8 col-sm-16 col-xs-16 order-content text-center">
			<table>
				<?php
				foreach ( $products['products'] as $i => $product ) {
					?>
					<tr>
						<td><?php echo $product['name']?></td>
						<td class="price">
							<?php echo $products['counts'][$product['id']];?> <em>&times;</em> <?php echo $this->getFormattedPrice($product['price'])?> / <?php echo CHtml::link( Yii::t('app', 'Удалить'), array('order/remove', 'pid'=>$product['id']), array('class'=>'order-remove') );?>
						</td>
					</tr>
				<?php } ?>
				<tr class="last">
					<td class="text-right">Итого:</td>
					<td class="price"><?php echo $this->getFormattedPrice($order['total']);?></td>
				</tr>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-md-16 col-sm-16 col-xs-16">
			<form role="form" id="order-form" class="order-form">
				<?php
				echo CHtml::activeTextField($model, 'fullname', array( 'placeholder'=>"Имя и Фамилия"));
				echo _drawErrorMessage($model, 'fullname');
				?>


				<?php
				echo CHtml::activeTextField($model, 'email', array( 'placeholder'=>"Email"));
				echo _drawErrorMessage($model, 'email');
				?>


				<?php
				echo CHtml::activeTextField($model, 'phone', array( 'placeholder'=>"Телефон: (098)555-55-55"));
				echo _drawErrorMessage($model, 'phone');
				?>


				<?php
				echo CHtml::activeTextArea($model, 'delivery_address', array( 'placeholder'=>"Адрес доставки: город, улица, дом/квартира"));
				echo _drawErrorMessage($model, 'delivery_address');
				?>

			</form>
		<div>
	</div>
	<div class="row">
		<div class="col-md-16">
			<?php echo CHtml::link(Yii::t('app', 'Отправить заказ'), array('order/settle'), array('class'=>'btn btn-primary btn-lg js-settle-order'));?>
		</div>
	</div>
	<?php } else { ?>
	<div class="row">
		<div class="col-md-16 col-sm-16 text-center"><?php echo Yii::t('app', 'Вы не выбрали ниодного товара');?></div>
	</div>
	<?php } ?>
</div>