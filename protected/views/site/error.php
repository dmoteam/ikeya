<?php
/**
 * @var $this SiteController
 * @var $code string
 * @var $message_title string
 * @var $message string
 */

$this->pageTitle = Yii::app()->name . ' - Error';
switch ($code) {
	case 404:
	default:
			$message_title = 'Не найдено';
			$message = '<p>К сожалению, по Вашему запросу мы ничего не нашли.</p>
			 <p>Попробуйте еще раз, или перейдите на <a href="'.Yii::app()->getBaseUrl( true ).'">главную страницу</a>.</p>';
		break;
}
?>
<div class="row error-info">
	<div class="col-sm-8 col-sm-offset-8 col-md-offset-8 col-md-8 description red-bg">
		<div class="row">
			<div class="col-md-12 clearfix message_title"><?php echo $message_title;?></div>
		</div>
		<div class="row">
			<div class="col-md-10 message">
				<?php echo //CHtml::encode(
					$message
				//);?>
			</div>
		</div>
	</div>
</div>