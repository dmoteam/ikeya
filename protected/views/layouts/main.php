<?php
/**
 * @var $this Controller
 */
$_img_base = Yii::app()->request->baseUrl .'/images/';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="favicon.ico">

	<title><?php echo Yii::app()->name . (!empty($this->pageTitle) ? ' / '. CHtml::encode($this->pageTitle):''); ?></title>

	<!-- Bootstrap core CSS -->
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/ikeya.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>
<div id="bootloader"></div>
<div class="container">

	<div class="row">
		<div class="col-md-8 col-sm-6 col-xs-6 col-lg-offset-1">
			<?php echo CHtml::link( CHtml::image($_img_base . 'logo.png', Yii::app()->name, array('class'=>'logo')), Yii::app()->getBaseUrl(true) );?> <span class="header-phone dark-grey">+38 (097) <span class="red">778-17-78</span></span>
		</div>
		<div class="col-md-6 col-sm-8 col-xs-8 text-right header-cart">
			<a href="<?php echo Yii::app()->createUrl('order/view')?>" title="Заказ" class="js-cart-link"><span class="cart-count red"><?php echo $this->order['count']?></span><span class="skew-line dark-grey-bg"></span><sup class="price"><?php echo $this->order['total']?></sup></a>
		</div>
	</div>

	<div class="row">
		<div class="col-md-16 col-lg-16 col-sm-16 col-xs-16" id="carousel-container">
			<div id="main-owl-carousel">
			<?php foreach ( Category::model()->findAll(array('order'=>'sort DESC')) as $category ) { ?>
				<img onclick="preLoadCategory('<?php echo 'category/'.$category->slug; ?>');" src="<?php echo $_img_base . $category->image; ?>">
				<?php //echo CHtml::image( $_img_base . $category->image, $category->name); ?>
				<?php// echo CHtml::link( CHtml::image( $_img_base . $category->image, $category->name), array('category/'.$category->slug), array('class'=>'category-link', 'title'=>CHtml::encode($category->name)) );?>
			<?php } ?>
			</div>


			<div class="ya-separator text-center col-md-10 col-md-offset-3 col-xs-12 col-xs-offset-2"><span></span></div>

		</div>
	</div>

<!--	CONTENT -->
	<div id="main-content">
		<?php echo $content; ?>
	</div>

<!--	END CONTENT -->

	<div class="ya-separator text-center col-md-10 col-md-offset-3 col-xs-12 col-xs-offset-2"><span></span></div>

	<div class="row footer">
		<div class="col-lg-offset-1 col-md-6 col-sm-6 col-xs-6 copyright">Copyright <?php echo date('Y');?></div>
		<div class="col-md-4 col-sm-4 col-xs-4 text-center"><?php echo CHtml::image(Yii::app()->getBaseUrl() . '/images/small_ikeya.png', 'ІКЕЯ');?></div>
		<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-right"><a href="http://dmo1.net/"><?php echo Yii::t('app', 'сделано в DMO');?></a></div>
	</div>

<!--     POPUP -->
	<div id="popup" style="display: none">
		<div id="popup-content">
			<?php
			//$this->renderPartial('//product/view', array('model'=>Product::model()->findBySlug('stoly-privet-2')))
			?>
		</div>
		<a href="#" id="close-popup" class="orange-bg">
			&times;
			<span><?php echo Yii::t('app', 'Закрыть');?></span>
		</a>
	</div>
<!--     POPUP END // -->

<!--	ERROR -->
	<div id="error404" style="display: none">
		<?php $this->renderPartial('/site/error', array('code'=>'404'));?>
	</div>
<!--	ERROR END // -->

</div> <!-- /container -->
<script>
	var baseUrl = '<?php echo Yii::app()->getBaseUrl(true);?>/',
		siteTitle = '<?php echo str_replace("'", "\'", Yii::app()->name);?>',
		strings = {
			ru: {
				cat: '<?php echo str_replace("'", "\'", Yii::t('app', 'категория'));?>',
				prod: '<?php echo str_replace("'", "\'", Yii::t('app', 'товар'));?>',
				order: '<?php echo str_replace("'", "\'", Yii::t('app', 'Ваш заказ отправлен'));?>'
			}
		};


</script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/owl.carousel.min.2.0.0.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.infinitescroll.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.history.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/iscroll.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/drag-on.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>

</body>
</html>