<?php
/**
 * @var $this CategoryController
 * @var array $newProducts
 */
?>
<!-- HITS -->
<!-- row END // -->
<div class="row hits">
	<div class="col-md-4 vmiddle col-md-offset-2 col-md-offset-2 col-sm-offset-1  col-sm-5 col-xs-16">
		<?php echo CHtml::image(Yii::app()->getBaseUrl(true). '/images/new_products.png', Yii::t('app', 'Новинки'), array('class'=>'img-responsive')) . "\n";?>
	</div>
	<?php
	$i = 1;
	foreach ( $newProducts as $product ) {
		if ($i % 3 == 0) {
			echo " <!-- row --> \n".
			     '<div class="row hits">' . "\n";
		}

		echo '  <div class="vmiddle col-md-4 ' . ( ( $i % 3 == 0 ) ? 'col-md-offset-2 col-sm-offset-1' : '' ) . ' col-sm-5 col-xs-16">' . "\n";
		// echo '  <div class="col-md-4 vmiddle product-lll '.(($i % 3 == 0)?'col-md-offset-2':'').'">'. "\n";
		$this->renderPartial('_product', array('data'=>$product));
		echo '  </div>' . "\n";

		$i++;
		if ($i % 3 == 0 || $i == count($newProducts)+1) {
			echo "</div>\n<!-- row END // -->\n\n";
		}
	}
	?>

	<!-- HITS END // -->