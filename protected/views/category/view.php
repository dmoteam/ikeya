<?php
/** @var $this CategoryController
 * @var $model Category
 * @var $dataProvider CActiveDataProvider
 */

$this->pageTitle =  mb_strtoupper(Yii::t('app', 'категория') . " / " .$model->name , 'UTF-8');
?>

<h1 class="category-title yellow text-center"><?php echo $model->name ?><a
		href="<?php echo Yii::app()->getBaseUrl( true ); ?>" class="category-close">X</a></h1>

<div style="margin-bottom: 30px;" class="ya-separator text-center col-md-10 col-md-offset-3 col-xs-12 col-xs-offset-2"><span></span></div>

<div id="category-contents">
	<?php
	$i        = 0;
	$products = $dataProvider->getData();


	foreach ( $products as $product ) {
		if ( $i % 3 == 0 ) {
			echo " <!-- row --> \n" . '<div class="row">' . "\n";
		}

		echo '  <div class="col-md-4 ' . ( ( $i % 3 == 0 ) ? 'col-md-offset-2 col-sm-offset-1' : '' ) . ' col-sm-5 col-xs-16">' . "\n";
		$this->renderPartial( '_product', array( 'data' => $product ) );
		echo '  </div>' . "\n";

		$i ++;
		if ( $i % 3 == 0 || $i == count( $products ) + 1 ) {
			echo "</div>\n<!-- row END // -->\n\n";
		}
	}

	?>
</div>
<?php
$this->widget( 'CLinkPager',
	array(
		'id' => 'pager',
		'pages' => $dataProvider->getPagination(),
		'header' => '',
		'footer' => '',
	)
);
?>