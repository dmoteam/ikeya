<?php
/**
 * @var $this CategoryController
 * @var $data Product
 */
?>
	<!-- PRODUCT -->
	<div class="product-item<?php echo (count($data->productProperties) > 0)?'':' noprops'; //if ($data->id == 8) echo ' expanded';//DEBUG ?>">
		<a href="<?php echo Yii::app()->createUrl('product/'.$data->slug)?>" title="<?php echo CHtml::encode($data->name);?>">
		<img src="<?php echo Yii::app()->getBaseUrl().'/images/products/'.$data->image?>" class="img-responsive" alt="<?php echo CHtml::encode($data->name);?>">
		<p>
			<span class="name">
				<?php echo CHtml::encode($data->name);?>
				<em class="clearfix"><?php echo CHtml::encode($data->type);?></em>
			</span>
			<span class="skew-line x2 dark-grey-bg"></span><sub class="price"><?php echo $this->getFormattedPrice($data->price);?></sub>
		</p>
		</a>
		<div class="row extra">
			<div class="col-md-8 col-sm-8 col-xs-16">
				<ul class="properties">
					<?php
					/* @var $property ProductProperties */
					foreach ( $data->productProperties as $property ) {
						echo sprintf('<li>%s: %s</li>', CHtml::encode( $property->property_name), CHtml::encode( ( (int)preg_replace('/[^0-9]*/', '', $property->property_value) == 0)? '-' : $property->property_value) );
					}?>
				</ul>
			</div>
			<div class="col-md-8 col-sm-8 col-xs-16 price-large">
				<?php if (!empty($data->special_price)) { ?>
					<span class="price-special"><?php echo $this->getFormattedPrice($data->special_price);?></span>
					<span class="price-normal">&#8594; <?php echo $this->getFormattedPrice($data->price);?></span>
				<?php } ?>
			</div>
		</div>
		<div class="pull-right extra">
			<?php
			echo CHtml::link( Yii::t('app', 'Купить'), array('order/add', 'pid'=>$data->id), array('class'=>'btn btn-primary js-add-to-cart') );
			?>
		</div>
	</div>
	<!-- PRODUCT END // -->
