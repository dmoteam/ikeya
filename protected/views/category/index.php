<?php
/**
 * @var $this CategoryController
 * @var array $hitsProducts
 * @var array $newProducts ,
 */

$this->pageTitle = '';

//Хиты продаж
$this->renderPartial( '_hits', array(
	'hitsProducts' => $hitsProducts,
) );

//Новые товары
$this->renderPartial( '_new', array(
	'newProducts'  => $newProducts,
) );

