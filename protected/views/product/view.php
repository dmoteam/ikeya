<?php
/** @var $this ProductController
 * @var $model Product
 */

$this->pageTitle =  mb_strtoupper(Yii::t('app', 'товар') . " / " .$model->name, 'UTF-8');

?>
<div id="view_detailed">	
	<div id="view_description">
		<div class="row">
			<div class="col-md-12 clearfix name"><?php echo $model->name;?></div>
		</div>
		<div class="row">
			<div class="col-md-16 price red"><?php echo $this->getFormattedPrice($model->price);?></div>
		</div>
		<div class="row">
			<div class="col-md-16 price red">
				<?php
				echo CHtml::link( Yii::t('app', 'Купить'), array('order/add', 'pid'=>$model->id), array('class'=>'btn btn-primary btn-lg js-add-to-cart') );
				?>
			</div>
		</div>
		<div class="row description-content">
			<div class="col-md-10 col-sm-10 col-xs-10">
				<?php echo nl2br(CHtml::encode($model->description));?>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-6">
				<table class="properties">
					<?php
					/* @var $property ProductProperties */
					foreach ( $model->productProperties as $property ) {
						echo sprintf('<tr><td>%s:</td><td>%s</td></tr>', CHtml::encode( $property->property_name), CHtml::encode( $property->property_value) );
					}?>
				</table>
			</div>
		</div>
	</div>

	<div id="view_photos">
		<?php
			foreach ( $model->productImages as $image ) {
				echo CHtml::image( Yii::app()->getBaseUrl() . '/images/products/' . $image->file );
			}
		?>
	</div>
</div>