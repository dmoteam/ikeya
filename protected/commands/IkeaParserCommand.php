<?php

Yii::import('application.extensions.behaviors.SluggableBehavior.Doctrine_Inflector');
require_once Yii::getPathOfAlias('application.vendor.ImageResize') .'/ImageResize.php';
require_once Yii::getPathOfAlias('application.vendor.phpQuery') .'/phpQuery-onefile.php';

class IkeaParserCommand extends CConsoleCommand
{
    private $_categories = array(
        //Диваны и кресла
        array('id'=>1, 'url' => 'http://www.ikea.com/ru/ru/catalog/categories/departments/living_room/10705/'), // Столы  (Столы)
        array('id'=>1, 'url' => 'http://www.ikea.com/ru/ru/catalog/categories/departments/workspaces/20649/'), //  Столы  (Столы)
        array('id'=>2, 'url' => 'http://www.ikea.com/ru/ru/catalog/categories/departments/workspaces/20652/'), //  Офисные стулья (стулья)
        array('id'=>3, 'url' => 'http://www.ikea.com/ru/ru/catalog/categories/departments/living_room/16239/'), // Кресла  (кресла)
        array('id'=>4, 'url' => 'http://www.ikea.com/ru/ru/catalog/categories/departments/living_room/10661/'), // Текстильная обивка  (диваны)
        array('id'=>4, 'url' => 'http://www.ikea.com/ru/ru/catalog/categories/departments/living_room/10662/'), // Кожаная обивка  (диваны)
        array('id'=>4, 'url' => 'http://www.ikea.com/ru/ru/catalog/categories/departments/living_room/10663/'), // Диваны-кровати  (диваны)
        array('id'=>4, 'url' => 'http://www.ikea.com/ru/ru/catalog/categories/departments/living_room/16238/'), // Модульные диваны  (диваны)
        //TODO more
    );

    private $_productCategories = array();

    private $_productList = array();

    public function run(){


        Yii::app()->db->createCommand( "truncate product;truncate product_image;truncate product_properties;" )->execute();

        foreach ($this->_categories as $_category ) {

            phpQuery::newDocumentHTML( $this->retrieveDocument( $_category['url'], 3 ) );


            foreach ( pq( '.threeColumn.product a' ) as $_product ) {
                $url = 'http://www.ikea.com' . pq( $_product )->attr( 'href' );
                if ( preg_match( '/products\//', $url ) ) {
                    $this->_productList[] = $url;
                    $this->_productCategories[$url] = $_category['id'];
                }
            }
            phpQuery::unloadDocuments();
        }

        $this->_productList = array_unique( $this->_productList );

        //print_r($this->_productList);


        $i = 1;
        foreach ( $this->_productList as $product_url ) {
            echo "\rParsed #$i";
            $this->parseProduct( $product_url );
            $i++;

            //if ($i > 3) break;
        }
        phpQuery::unloadDocuments();

        echo "\nDone\n";

    }

    /**
     * get file content either into /runtime/cache/sha1(url).ikea
     * or
     * return as a string
     *
     * @param string $url
     * @param integer $return   1- return, 2 - save to cache only, 3 - save to cache and return content
     * @param integer $save_location   custom save location
     * @param integer $original_name  keep original name
     * @return string
     */
    private function retrieveDocument($url, $return=1, $save_location=false, $original_name = false) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $content = curl_exec($ch);
        curl_close($ch);

        if (in_array($return, array(2,3))) {
            //TODO error reporting
            if (!$save_location) {
                file_put_contents( $this->_createCachedFileName($url), $content );
            } else {
                file_put_contents( $save_location . '/' . $this->_createCachedFileName($url, false, $original_name), $content );
            }
        }

        if (in_array($return, array(1,3))) {
            return $content;
        }
    }

    public function parseProduct($url) {
        echo "\nparsing $url :\n";

        try {

            $html_cache_file = $this->_createCachedFileName( $url );
            if ( file_exists( $html_cache_file ) ) {
                $CONTENT = file_get_contents( $html_cache_file );
            } else {
                $CONTENT = $this->retrieveDocument( $url, 3 );
            }

            preg_match(
                '/var jProductData = \{(.*)\};/',
                $CONTENT,
                $matches
            );

            if (empty($matches)) return ;

            $product_data = json_decode( '{' . $matches[1] . '}' );
            //echo print_r($product_data, 1);

            $product = array(
                'product_id'    => 0,
                'category_id'   => $this->_productCategories[$url],
                'name'          => (string) $product_data->product->items[0]->name,
                'type'          => (string) $product_data->product->items[0]->type,
                'slug'          => '',
                'image'         => $product_data->product->items[0]->images->large[0],
                'price'         => (string) $product_data->product->items[0]->prices->normal->priceNormal->rawPrice * 0.305117243,
                'special_price' => 0,
                'description'   =>
                    strip_tags(
                        preg_replace(
                            '/\<br\s?\/?\>/', "\n",
                            (string) $product_data->product->items[0]->goodToKnow . "\n" .
                            (string) $product_data->product->items[0]->careInst . "\n" .
                            (string) $product_data->product->items[0]->custMaterials
                        )
                    )
            ,
                'extra_images'  => $product_data->product->items[0]->images->large,
                'properties'    => (array) $product_data->product->items[0]->metricPackageInfo[0],
                'ikea_link'     => $url,
            );

            // SLUG
            $slug = $checkslug = Doctrine_Inflector::urlize(
                implode( '-', array( Yii::app()->transliteration->transliterate( $product['name'] ) ) ) // todo add category name
            );

            // unique slug
            $counter = 0;
            while ( Product::model()->resetScope()->findByAttributes( array( 'slug' => $checkslug ) )
            ) {
                Yii::trace( "$checkslug found, iterating", __CLASS__ );
                $checkslug = sprintf( '%s-%d', $slug, ++ $counter );
            }
            $product['slug'] = $counter > 0 ? $checkslug : $slug;
            // SLUG END

            if ( ! empty( $product['name'] ) ) {
                $this->recordProduct( $product );
            } //save to db

        } catch (Exception $e) {
            echo "\n\n==================\n\n". print_r($e, 1)."\n\n==================\n\n";
        }

    }

    private function _createCachedFileName($url, $with_path = true, $original_filename = false) {
        return ( $with_path ? Yii::getPathOfAlias( 'application.runtime.cache' ) . '/' : '' ) . (!$original_filename ? sha1( $url ) . '.ikea' : basename($url));
    }

    private function recordProduct($product_data) {
        $product = new Product();

        $product_data['image'] = basename($product_data['image']);

        $product->attributes = $product_data;

        //print_r($product_data);

        //print_r($product_data);
        //print_r($product->attributes );

        if ( $product->save() ) {
            foreach ( $product_data['properties'] as $name => $value ) {
                $property = new ProductProperties();
                switch ($name) {
                    case "length":
                            $property->property_name = 'Длина';
                            $property->property_value = $value . ' см';
                        break;
                    case "width":
                            $property->property_name = 'Ширина';
                            $property->property_value =  $value . ' см';
                        break;
                    case "height":
                            $property->property_name = 'Высота';
                            $property->property_value =  $value . ' см';
                        break;
                    case "weight":
                        $property->property_name = 'Вес';
                        $property->property_value =  $value . ' г';
                        break;
                }
                $property->product_id = $product->id;
                if (!empty($property->property_name) && !empty($property->property_name)) $property->save();
                //print_r($property->getErrors());
            }

            foreach ( $product_data['extra_images'] as $i => $image ) {
                $product_image = new ProductImage();
                $this->retrieveDocument('http://www.ikea.com/' . $image, 3, dirname(Yii::app()->getBasePath()) . '/images/products/', true);


                $product_image->file = basename($image);
                /*$imagename_parts = array(
                    pathinfo($product_image->file, PATHINFO_FILENAME),
                    pathinfo($product_image->file, PATHINFO_EXTENSION),
                );
                $image = new Eventviva\ImageResize(dirname(Yii::app()->getBasePath()) . '/images/products/' . $product_image->file );
                $image->resizeToHeight(253);
                $image->save(dirname(Yii::app()->getBasePath()) . '/images/products/' . $imagename_parts[0] .'x253.' . $imagename_parts[1] );*/

                $product_image->product_id = $product->id;
                if (!empty($product_image->file) && file_exists(dirname(Yii::app()->getBasePath()) . '/images/products/' . $product_image->file)) {
                    $product_image->save();
                    //update product
                    //$product->setAttribute('image')
                }
                //print_r($product_image->getErrors());
            }
        } else {
            //print_r($product->getErrors());
        }

        echo "Written product {$product->name} [$product->category_id]\n\n";

        //print_r($product->getErrors());
    }

    private function _cleanHtml($string) {
       /* return
            trim(
                preg_replace(
                    '/[\t]+/', ' ',
                    preg_replace('/\n+|\r+/', "\n",
                        preg_replace('/\n+|\r+/', "\n",
                            strip_tags(
                                preg_replace(
                                    "/\<\/?(div|p|br).*\>/i",
                                    "\n",
                                    preg_replace(
                                        "(<(a).*?>.*?</\\1>)is",
                                        '',
                                        $string
                                    )
                                )
                            )
                        )
                    )
                )
            )
        ;*/
    }

    private function _parseProperties($string) {
        $ret = explode("<br>", $string);
        foreach ( $ret as $i => $_prop ) {
            $ret[ $i ] = trim( $ret[ $i ] );
            if ( empty( $ret[ $i ] ) ) unset($ret[$i]);
        }

        foreach ( $ret as $i => $_prop ) {
            $ret[$i] = explode(':', $_prop);
            $ret[$i]['property_name'] = trim($ret[$i][0]); // property name
            $ret[$i]['property_value'] = trim($ret[$i][1]); //property value
            unset($ret[$i][0]);unset($ret[$i][1]);


        }
        return $ret;
    }
}