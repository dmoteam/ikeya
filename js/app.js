/**
 *  @var baseUrl
 *  @var siteTitle
 *  @var strings
 */

 /* Modernizr 2.8.3 (Custom Build) | MIT & BSD
 * Build: http://modernizr.com/download/#-touch-teststyles-prefixes
 */
;window.Modernizr=function(a,b,c){function v(a){i.cssText=a}function w(a,b){return v(l.join(a+";")+(b||""))}function x(a,b){return typeof a===b}function y(a,b){return!!~(""+a).indexOf(b)}function z(a,b,d){for(var e in a){var f=b[a[e]];if(f!==c)return d===!1?a[e]:x(f,"function")?f.bind(d||b):f}return!1}var d="2.8.3",e={},f=b.documentElement,g="modernizr",h=b.createElement(g),i=h.style,j,k={}.toString,l=" -webkit- -moz- -o- -ms- ".split(" "),m={},n={},o={},p=[],q=p.slice,r,s=function(a,c,d,e){var h,i,j,k,l=b.createElement("div"),m=b.body,n=m||b.createElement("body");if(parseInt(d,10))while(d--)j=b.createElement("div"),j.id=e?e[d]:g+(d+1),l.appendChild(j);return h=["&#173;",'<style id="s',g,'">',a,"</style>"].join(""),l.id=g,(m?l:n).innerHTML+=h,n.appendChild(l),m||(n.style.background="",n.style.overflow="hidden",k=f.style.overflow,f.style.overflow="hidden",f.appendChild(n)),i=c(l,a),m?l.parentNode.removeChild(l):(n.parentNode.removeChild(n),f.style.overflow=k),!!i},t={}.hasOwnProperty,u;!x(t,"undefined")&&!x(t.call,"undefined")?u=function(a,b){return t.call(a,b)}:u=function(a,b){return b in a&&x(a.constructor.prototype[b],"undefined")},Function.prototype.bind||(Function.prototype.bind=function(b){var c=this;if(typeof c!="function")throw new TypeError;var d=q.call(arguments,1),e=function(){if(this instanceof e){var a=function(){};a.prototype=c.prototype;var f=new a,g=c.apply(f,d.concat(q.call(arguments)));return Object(g)===g?g:f}return c.apply(b,d.concat(q.call(arguments)))};return e}),m.touch=function(){var c;return"ontouchstart"in a||a.DocumentTouch&&b instanceof DocumentTouch?c=!0:s(["@media (",l.join("touch-enabled),("),g,")","{#modernizr{top:9px;position:absolute}}"].join(""),function(a){c=a.offsetTop===9}),c};for(var A in m)u(m,A)&&(r=A.toLowerCase(),e[r]=m[A](),p.push((e[r]?"":"no-")+r));return e.addTest=function(a,b){if(typeof a=="object")for(var d in a)u(a,d)&&e.addTest(d,a[d]);else{a=a.toLowerCase();if(e[a]!==c)return e;b=typeof b=="function"?b():b,typeof enableClasses!="undefined"&&enableClasses&&(f.className+=" "+(b?"":"no-")+a),e[a]=b}return e},v(""),h=j=null,e._version=d,e._prefixes=l,e.testStyles=s,e}(this,this.document);

loader = new Image();
loader = new Image();
$(loader).addClass('loader');
loader.src = baseUrl + '/images/ajax-loader.gif';
var     History = window.History,
        State = History.getState(),
        owl,
        productImagesScroll,
        categoryInfiniteScroll;

function destroyCategoryScroll(){
    try {
        categoryInfiniteScroll.infinitescroll('destroy');

        categoryInfiniteScroll.data('infinitescroll', null);
    } catch(e){}
}

function applyScroll(){
    categoryInfiniteScroll = $('#category-contents');

    destroyCategoryScroll();

    var maxpage = 0;

    try {
        maxpage = parseInt(categoryInfiniteScroll.next('#pager').find('li.last a').attr('href').match(/\d+$/).pop());
    } catch (e) {
        maxpage = 0;
    }

    categoryInfiniteScroll.infinitescroll({
        navSelector: "#pager",
        nextSelector: "#pager li.next a",
        contentSelector: "#category-contents",
        itemSelector: "#category-contents > .row",
        //debug: true,
        dataType: 'html',
        maxPage: maxpage,
        loading: {
            finishedMsg: "",
            msgText: "",
            speed: 'fast',
            img: loader.src
        },
        bufferPx: 40
    });
}

function loadToPopup(_link){
    //console.log('Loading popup ... ' + _link);
    $.ajax({
        url: _link,
        type: 'get', // 'POST' | 'GET'
        dataType: 'html', //xml, html, script, json, jsonp, text
        beforeSend: function (xhr) {
            buildLoader();
            $('body').css({'overflow': 'hidden'});
            $("#popup-content").html(loader);
            $("#popup").fadeIn();//xhr.overrideMimeType("text/plain; charset=x-user-defined");
        },
        cache: true, //default: true, false for dataType 'script' and 'jsonp'
        statusCode: {
            404: function () {
                $("#popup-content").html( $('#error404').html() );
            }
        }
    }).done(function (data, textStatus, jqXHR) {
        $("#popup-content").html(data);
        if (_link.match(/product\//)) {
            if (!Modernizr.touch) { 
                $('#view_photos').dragOn();
            } 
            $('#popup #popup-content').css({'overflow-y': 'hidden'});
        }
        destroyLoader();
    }).fail(function (jqXHR, textStatus, errorThrown) {

    }).always(function (data, textStatus, jqXHR/*|errorThrown*/) {

    });
}

function loadCategory(_link) {
    $.ajax({
        url: _link,
        type: 'get', // 'POST' | 'GET'
        dataType: 'html', //xml, html, script, json, jsonp, text
        beforeSend: function (xhr) {
            buildLoader();
        },
        cache: true, //default: true, false for dataType 'script' and 'jsonp'
    }).done(function (data, textStatus, jqXHR) {
        $("#main-content").html(data);
        applyScroll();
        $('#carousel-container').fadeOut(200);
        $("#popup").fadeOut(300, function(){});
        $('body').css({'overflow': 'scroll'});
        $('#popup #popup-content').css({'overflow-y': 'scroll'});
        destroyLoader();
    })
}


// _TO_REMOVE
// function loadCategory_old(_link) {
//    alert('');
//    $('#main-content').load( _link, function(){
//        applyScroll();
//    });
//    $('#carousel-container').fadeOut(200);
//    $("#popup").fadeOut(300, function(){});
//    $('body').css({'overflow': 'scroll'});
//    $('#popup #popup-content').css({'overflow-y': 'scroll'});
// }



function loadHome(){
    buildLoader();
    $('#carousel-container').fadeIn(200);
    $('#main-content').fadeOut(200, function(){
        $('#main-content').load( baseUrl, function(){
            $('#main-content').fadeIn(200);
            $("#popup").fadeOut(300, function(){});
            $('body').css({'overflow': 'scroll'});
            $('#popup #popup-content').css({'overflow-y': 'scroll'});
            destroyLoader();
        });
    });
}

function buildLoader() {
    $('#bootloader').fadeIn(100);
}

function destroyLoader() {
    setTimeout(function() {
        $('#bootloader').fadeOut(500);
    }, 300);
    // $('#bootloader').fadeOut(100);
}

function loadStateOf(_link){
    if (typeof _link != 'undefined' && _link.length > 0) {
        console.log( 'Changing state to ' + _link)

        // back / forward button to category page - load it via ajax
        if (_link.match(/category\//)) {
            console.log( '   ... which is category' );
            loadCategory(_link);
        } else

        // back / forward button to product/order page - load it via ajax
        if (_link.match(/product\/|order\//)) {
            console.log( '   ... which is product / order' );
            loadToPopup(_link);
        } else

        // back / forward button to home page - load it via ajax
        if (_link == baseUrl) {
            console.log( '   ... which is home' );
            loadHome();
        }
    }
}

$(document).ready(function () {

    $('body').on('keyup', function(e){
        if(e.keyCode == 27){
            $('#close-popup').trigger('click');
        }
    }).on('click', 'a.js-add-to-cart', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        el = this;

        $.ajax({
          url: $(el).attr('href'),
          data: {},
          type: 'GET', // 'POST' | 'GET'
          dataType: 'json', //xml, html, script, json, jsonp, text
          cache: false
        }).done(function ( data, textStatus, jqXHR ) {
            //TODO error checks
            $('.header-cart .cart-count').html(data.count);
            $('.header-cart .price').html(data.total);
            if ( $('#popup').is(':visible') ) {
                $('#close-popup').trigger('click');
                loadToPopup('order/view');
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            //TODO
        });

    }).on('click', 'a.category-close', function(e){
        e.preventDefault();
        e.stopPropagation();

        el = this;
        var _link = baseUrl;
        History.pushState({state:1,rand:Math.random()}, siteTitle, _link);
        destroyCategoryScroll();
        owl.trigger('replace.owl.carousel', [{touchDrag: false, mouseDrag: false}]);

    }).on('click', '.product-item a, .header-cart a', function (e) {
        e.preventDefault();
        e.stopPropagation();
        el = this;
        var _link = $(el).attr('href');
        var _prodname = $(el).attr('title');
        History.pushState({state:1,rand:Math.random()}, siteTitle + " / " + (!$(el).hasClass('js-cart-link')? strings.ru.prod.toUpperCase() + " / " : '' ) + _prodname.toUpperCase(), _link);
        //loadStateOf(_link);
    }).on('click', 'a.order-remove', function (e) {
        e.preventDefault();
        e.stopPropagation();
        el = this;

        $.ajax({
          url: $(el).attr('href'),
          type: 'GET', // 'POST' | 'GET'
          dataType: 'json', //xml, html, script, json, jsonp, text
          cache: false
        }).done(function ( data, textStatus, jqXHR ) {

            console.log( data );
            //TODO error checks
            $('.header-cart .cart-count').html(data.count);
            $('.header-cart .price').html(data.total);
            $('#popup-content').load(baseUrl + '/order/view');
        }).fail(function(jqXHR, textStatus, errorThrown) {
            //TODO
        });
    }).on('click', 'a.js-settle-order', function (e) {
        e.preventDefault();
        e.stopPropagation();
        el = this;

        $('#order-form .error').each(function(){
            $(this).html('');
        });

        $.ajax({
          url: $(el).attr('href'),
          type: 'POST', // 'POST' | 'GET'
          data: $('#popup #order-form').serialize(),
          dataType: 'json', //xml, html, script, json, jsonp, text
          cache: false
        }).done(function ( data, textStatus, jqXHR ) {
            if (typeof data.status == 'undefined' || data.status == 'error') {
                $.each(
                    data.data,
                    function (key, val) {
                        console.log($("#" + key + "_em_"));
                        console.log("#" + key + "_em_");
                        $("#popup #" + key + "_em_").text(val);
                        $("#popup #" + key + "_em_").show();
                    }
                );

            } else {
                $('.header-cart .cart-count').html(data.data.count);
                $('.header-cart .price').html(data.data.total);
                $('.order-content').html(
                    //'<div class="row"><div class="col-md-16 col-sm-16 text-center">'+
                    strings.ru.order
                    //+'</div></div>'
                );
                setTimeout(function(){
                    $('#close-popup').trigger('click');
                }, 2000);
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            //TODO
        });
    });

    $('#close-popup').on('click', function(e){
        e.preventDefault();
        e.stopPropagation();
        $("#popup").fadeOut(300, function(){});
        //console.log( History.getStateByIndex( History.getCurrentIndex()-1) );
        var _prevState = History.getStateByIndex( History.getCurrentIndex()-1);

        //if previous state is same - we'll have ''close loop', so change state to home page instead. TODO more intelligent solution
        if (_prevState.url == window.location.href ) {
            _prevState.url = baseUrl;
            _prevState.title = siteTitle;
        }
        History.pushState(
            {state:1,rand:Math.random()},
            _prevState.title,
            _prevState.url
        );
    });

    $('body').on('click', '.category-link', function(e){
        e.preventDefault();
        e.stopPropagation();

        console.log( ' WTF');

        var _link = $(this).attr('href');
        var _catname = $(this).attr('title');
        History.pushState({state:1,rand:Math.random()}, siteTitle + " / " + strings.ru.cat.toUpperCase() + " / " + _catname.toUpperCase(), _link);
        loadStateOf(_link);
    });
});

// Bind to State Change
History.Adapter.bind(window,'statechange',function(){
    loadStateOf(window.location.href);
});

var scrollLeft = 0;

$(window).on('load', function() {
    History.pushState({state:1,rand:Math.random()}, document.title, window.location.href);//initial state push to allow 'back' browser button
    History.log('initial:', State.data, State.title, State.url);

    $('img').on('dragstart', function(event) { event.preventDefault(); });

    $('#main-owl-carousel').mousedown(function(e) {
        scrollLeft = $('#main-owl-carousel').scrollLeft();
    });

    
    if (!Modernizr.touch) { 
        $('#view_photos').dragOn();
    }

    if (Modernizr.touch) {
        $('#main-owl-carousel').css({'overflow-x': 'scroll'});
    } else {
        $('#main-owl-carousel').dragOn();
    }

    applyScroll();
});

function preLoadCategory(_link) {
    if (Modernizr.touch) { 
        loadCategory(_link);
    }

   if($('#main-owl-carousel').scrollLeft() == scrollLeft) {
   		window.location.href = _link;
        // loadCategory(_link);
   }
}